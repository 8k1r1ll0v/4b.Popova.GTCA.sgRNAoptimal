# -*- coding: utf-8 -*-
"""
Created on Sun May 22 13:08:51 2016

@author: k1r1ll0v
"""

from sklearn.neural_network import MLPClassifier
from sklearn.neural_network import MLPRegressor
from sklearn.externals import joblib
import argparse
import os.path as p


def generateInitials(directory, seed):
    D = MLPClassifier(
        algorithm="sgd", learning_rate="adaptive",
        random_state=seed, warm_start=True,
        early_stopping=True
    )
    G = MLPRegressor(
        algorithm="sgd", learning_rate="adaptive",
        random_state=seed, warm_start=True,
        early_stopping=True
    )
    E = MLPRegressor(
        algorithm="sgd", learning_rate="adaptive",
        random_state=seed, warm_start=True,
        early_stopping=True
    )
    joblib.dump(G, p.join(directory, "G.bin"))
    joblib.dump(D, p.join(directory, "D.bin"))
    joblib.dump(E, p.join(directory, "E.bin"))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description = "Generate initial models"
    )
    parser.add_argument(
        "directory",
        metavar="Directory",
        help="Directory with G.bin and D.bin",
        action="store"
    )
    parser.add_argument(
        "--seed",
        metavar="PRG seed",
        action="store",
        help="Pseudorandom number generator seed",
        default="666"
    )
    args = parser.parse_args()
    generateInitials(args.directory, int(args.seed))