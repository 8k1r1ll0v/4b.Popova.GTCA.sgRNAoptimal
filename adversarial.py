# -*- coding: utf-8 -*-
"""
Created on Sun May 22 04:50:58 2016

@author: k1r1ll0v
"""

from sklearn.externals import joblib
import numpy as np
import os.path


class Estimator():

    def __init__(self, d):
        self.E = joblib.load(d)
        self.log = []

    def estimate(self, d, encode):
        self.log.append(int(self.E.predict([encode(d).uint])))

    def fit(self, d, r):
        self.E.fit(d, r)

    def save(self, d):
        joblib.dump(self.E, os.path.join(d, "E.bin"))


class Generator():

    def __init__(self, g):
        self.G = joblib.load(g)
        self.log = []

    def generate(self, rank):
        strand = self.G.predict(rank)
        self.log.append((rank, strand))


class GAN():

    def __init__(
        self, G, D,
        steps, dsteps, m, modelname,
        seed
    ):
        self.G = G #multilayer perceptron - Generative
        self.D = D #multilayer perceptron - Discriminative
        self.mname = modelname #Name for joblib
        if not os.path.exists(self.mname):
            os.mkdir(self.mname)
        self.steps = steps #steps
        self.dsteps = dsteps #steps for fitting D
        self.mbs = m #minibatch size
        np.random.seed(seed)

    def load(self):
        self.G = joblib.load(self.G)
        self.D = joblib.load(self.D)

    def fit(self, examples, noize):
        self.noize = noize #Noize == Percent-rank
        self.examples = examples #Examples == DNA strands in form of uints

    def fight(self):
        #Algorithm 1 by Goodfellow et al.
        nms = np.random.choice(range(len(self.noize)), self.mbs)
        nz = []
        ex = []
        for a in nms:
            nz.append([self.noize[a]])
            ex.append(self.examples[a])
        self.G.fit(nz, ex)
        for epoch in range(self.steps):
            for d_epoch in range(self.dsteps):
                noize = map(
                    lambda x: [x],
                    np.random.choice(self.noize, self.mbs).tolist()
                )
                examples = map(
                    lambda x: [x],
                    np.random.choice(self.examples, self.mbs).tolist()
                )
                generated = map(
                    lambda x: [int(abs(x))],
                    self.G.predict(noize).tolist()
                )
                classes = np.array(
                    [0]*len(examples)+[1]*len(generated)
                )
                allof = np.matrix(examples + generated)
                self.D.fit(
                        allof,
                        classes
                )
            nms = np.random.choice(range(len(self.noize)), self.mbs)
            nz = []
            ex = []
            for a in nms:
                nz.append([self.noize[a]])
                ex.append(self.examples[a])
            self.G.fit(nz, ex)
            if epoch % 1000 == 0:
                print "epoch #"+str(epoch)

    def save(self):
        fn_G = os.path.join(self.mname, "G.bin")
        fn_D = os.path.join(self.mname, "D.bin")
        joblib.dump(self.G, fn_G)
        joblib.dump(self.D, fn_D)