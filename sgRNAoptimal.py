# -*- coding: utf-8 -*-
"""
Created on Sat May 21 11:02:45 2016

@author: k1r1ll0v
"""
import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns # for pretty plots
from os import path
from adversarial import *
from bitstring import BitArray
import re

class Strand():

    tbl = {
        "A":[0,0], "T": [0,1], "C": [1,0], "G": [1,1]
    }
    revtbl = {
        "00": "A", "01": "T", "10": "C", "11": "G"
    }

    def __init__(self, strand):
        self.stringrep = strand
        self.body = Strand.encode(strand)

    @staticmethod
    def encode(strand):
        u = []
        for a in map(lambda x: Strand.tbl[x], strand):
            u.extend(a)
        b = BitArray(u)
        return b

    @staticmethod
    def decode(strand, length):
        bar = BitArray(uint=strand, length=length*2).bin
        i = 0
        nucl = []
        while i != len(bar):
            nucl.append(Strand.revtbl["".join([bar[i], bar[i+1]])])
            i += 2
        return "".join(nucl)

def startLearning(args):
    print "Initializing Generative Adversarial Network Learner"
    gan = GAN(
        args.G,
        args.D,
        int(args.steps), int(args.dsteps), int(args.batch_size),
        args.outputF, int(args.seed)
    )
    print "Fitting Generative Adversarial Network"
    gan.load()
    dataset = pd.read_csv(args.sampleF)
    frm = args.form.split(",")
    noize = dataset[frm[1]]
    examples = map(lambda x: Strand(x).body.uint, dataset[frm[0]])
    gan.fit(examples, noize)
    gan.fight()
    print "Saving models"
    gan.save()

def startEstimation(args):
    E = Estimator(
        args.E
    )
    if args.targetS[0:3] == "sg:":
        E.estimate(args.targetS[4:len(args.targetS)], Strand.encode)
    else:
        rnas = open(args.targetS, "r").read().split("\n")
        for a in rnas:
            E.estimate(a, Strand.encode)
    if args.outputF == "stdout":
        print(E.log)
    else:
        oh = open(args.outputF, "w")
        for a in E.log:
            oh.write(
                str(a[0])+","+Strand.decode(
                    a[1], int(args.length)
                )+"\n"
            )
        oh.close()

def startFitEstim(args):
    E = Estimator(
        args.E
    )
    dataset = pd.read_csv(args.sampleF)
    frm = args.form.split(",")
    noize = map(lambda x: [x], dataset[frm[1]].tolist())
    examples = map(lambda x: Strand(x).body.uint, dataset[frm[0]])
    E.fit(noize, examples)
    E.save(args.outputF)

def startGeneration(args):
    gen = Generator(
        path.join(args.G, "G.bin")
    )
    ranks = []
    if not re.match("^\d+\.*\d*$", args.rank):
        ranks = map(float, open(args.rank, "r").read().split("\n"))
    else:
        ranks.append(float(args.rank))
    for a in ranks:
        gen.generate(
            float(args.rank)
        )
    if args.outputF == "stdout":
        print (gen.log[-1][0],Strand.decode(abs(gen.log[-1][1]), int(args.length)))
    else:
        oh = open(args.outputF, "w")
        for a in gen.log:
            oh.write(
                str(a[0])+","+Strand.decode(
                    a[1], int(args.length)
                )+"\n"
            )
        oh.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description = "GAN can sgRNA"
    )
    parser.add_argument(
        "outputF",
        metavar="Output file",
        action="store",
        help="Output file"
    )
    parser.add_argument(
        "--input-mask",
        metavar="Mask",
        action="store",
        help="Mask of sequence",
        default="full" #TFTTFTFTF, T - include, F - exclude
    )
    subparsers = parser.add_subparsers(
        help="Modes of execution"
    )
    fit_parser = subparsers.add_parser(
        "fit", help="Fit an Estimator"
    )
    fit_parser.add_argument(
        "sampleF",
        metavar="Training sample",
        action="store",
        help="Sample.csv"
    )
    fit_parser.add_argument(
        "E",
        metavar="Estimation model",
        action="store",
        help="Model of estimator"
    )
    fit_parser.add_argument(
        "--form",
        metavar="Form of .csv",
        action="store",
        help=".csv column names",
        default="Sequence,Rank"
    )
    fit_parser.add_argument(
        "--seed",
        metavar="PRG seed",
        action="store",
        help="Pseudorandom number generator seed",
        default="666"
    )
    fit_parser.set_defaults(func=startFitEstim)
    learn_parser = subparsers.add_parser(
        "learn", help="Learn a Generative Adversarial Networks"
    )
    learn_parser.add_argument(
        "sampleF",
        metavar="Training sample",
        action="store",
        help="Sample.csv"
    )
    learn_parser.add_argument(
        "G",
        metavar="Generation model",
        action="store",
        help="Model of generator"
    )
    learn_parser.add_argument(
        "D",
        metavar="Discrimination model",
        action="store",
        help="Model of discriminator"
    )
    learn_parser.add_argument(
        "--form",
        metavar="Form of .csv",
        action="store",
        help=".csv column names",
        default="Sequence,Rank"
    )
    learn_parser.add_argument(
        "--steps",
        metavar="Steps",
        action="store",
        help="Number of fighting epochs",
        default="1000"
    )
    learn_parser.add_argument(
        "--dsteps",
        metavar="Discrimination steps",
        action="store",
        help="Number of discrimination steps",
        default="1"
    )
    learn_parser.add_argument(
        "--batch-size",
        metavar="Batch size",
        action="store",
        help="Minimum batch size",
        default="32"
    )
    learn_parser.add_argument(
        "--seed",
        metavar="PRG seed",
        action="store",
        help="Pseudorandom number generator seed",
        default="666"
    )
    learn_parser.set_defaults(func=startLearning)
    find_parser = subparsers.add_parser(
        "estimate", help="Estimate sgRNA performance"
    )
    find_parser.add_argument(
        "targetS",
        metavar="Target",
        action="store",
        help="Target sequence"
    )
    find_parser.add_argument(
        "E",
        metavar="Estimation model",
        action="store",
        help="Model of estimator"
    )
    find_parser.set_defaults(func=startEstimation)
    gen_parser = subparsers.add_parser(
        "generate", help="Generate sgRNA with given performance"
    )
    gen_parser.add_argument(
        "G",
        metavar="Generator",
        action="store",
        help="Path to generator"
    )
    gen_parser.add_argument(
        "rank",
        metavar="sgRNA rank",
        action="store",
        help="Rank of sgRNA (or file of ranks)"
    )
    gen_parser.add_argument(
        "--seed",
        metavar="PRG seed",
        action="store",
        help="Pseudorandom number generator seed",
        default="666"
    )
    gen_parser.add_argument(
        "--length",
        metavar="sgRNA length",
        action="store",
        help="sgRNA length",
        default="20"
    )
    gen_parser.set_defaults(func=startGeneration)
    args = parser.parse_args()
    args.func(args)