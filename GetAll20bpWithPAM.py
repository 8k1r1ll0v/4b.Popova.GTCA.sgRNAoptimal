sequence = "ATATATTATAGCGCGTATGCGCGGGAcCtttaaagggTATATGGGATGATAGGATAGAGTGG"
PAMdinucl = "GG"

'''
Created 21.05.2016 by Nick Zolotarev
'''
def MakeLettersBig (sequence):
    '''
    Return str with all letters in sequence converted to capitals. Characters exept A,T,G,C,a,t,g,c will be ignored.
    '''
    Letters = ("A", "T", "G", "C")
    letters = {"a":"A", "t":"T", "g":"G", "c":"C"}
    BigSeq = ""
    for i in sequence:
        if i in Letters:
            BigSeq += i
        if i in letters.keys():
            BigSeq += letters[i]
    return BigSeq

def MakeReverseCompliment (sequence):
    '''
    Return reverse compliment of sequence.
    '''
    ComplimentaryLetters = {"A":"T", "T":"A", "G":"C", "C":"G"}
    CompSeq = ""
    for i in sequence:
        CompSeq += ComplimentaryLetters[i]
    RevCompSeq = CompSeq [::-1]
    return RevCompSeq

def GetAll20bpWithPAM (sequence, PAMdinucl = "GG", Strand = "+"):
    '''
    Return list of tuples with sgRNA sequence, coordinate of first nucleotide of sgRNA in input sequence,
    and strand of sgRNA.
    :param PAMdinucl: str 2 last letters of PAM
    '''
    seqLen = len(sequence)
    sgRNAnum = seqLen - 22
    PAMdinuclCoord = 21
    sgRNAs = []
    for i in range(sgRNAnum):
        if sequence[PAMdinuclCoord:PAMdinuclCoord+2] == PAMdinucl:
            sgRNAs.append((sequence[i:i+20], i, Strand))
        PAMdinuclCoord += 1
    return sgRNAs

def FindAllsgRNAs (sequence, PAMdinucl):
    '''
    Return list of tuples with sgRNA sequence, coordinate of first nucleotide of sgRNA in input sequence,
    and strand of sgRNA. For any DNA sequence and reverse compliment.
    :param PAMdinucl: str 2 last letters of PAM
    '''
    BigSeq = MakeLettersBig(sequence)
    RevCompSeq = MakeReverseCompliment(BigSeq)
    sgRNAs = GetAll20bpWithPAM(BigSeq, PAMdinucl, Strand = "+")
    sgRNAs += GetAll20bpWithPAM(RevCompSeq, PAMdinucl, Strand = "-")
    return sgRNAs

print FindAllsgRNAs(sequence, PAMdinucl)